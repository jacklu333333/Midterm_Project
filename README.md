# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Puzzle
* Key functions (add/delete)
    1. user page
    2.  post page
    3. post list page
    4. leave comment under any post
* Other functions (add/delete)
    1. CSS animation
    2. Post Time
    3. facebook Like & Share 
    4. Facebook Save
    5. edit your profile/
    6. sign with facebook
    7. chrome notification

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|GitLab Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description

## Security Report (Optional)
為安全起見禁止在任何留言處輸入'<'或'>'字元以免植入fumction或影響排版，每個葉面皆有認證機制透過firebae確認使用者身分
